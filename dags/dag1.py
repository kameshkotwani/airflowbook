from airflow.operators.empty import EmptyOperator
from airflow import DAG
from pendulum import datetime
with DAG(
    dag_id="dag1",
    description="Just an empty Operator",
    schedule=None,
    start_date=datetime(2023,5,25)

) as main_dag:
    task1 = EmptyOperator(
        task_id="task1"
    )
    task2 = EmptyOperator(
        task_id="task2"
    )

    task1 >> task2